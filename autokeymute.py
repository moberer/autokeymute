#!/usr/bin/env python3

#################################################################################
# SCRIPT SETTINGS - Adapt to your system & your preferences
#################################################################################

# Pulseaudio Audio Input Device.
# Best retrieved using 'pactl list short sources'.
# Make sure this is set correctly or nothing will work!
AUDIODEVICE = "alsa_input.usb-BEHRINGER_UMC404HD_192k-00.multichannel-input"

# Volume levels. Adapt only if necessary
VOLUME_LOW  =  50 # volume in use when typing            (recommended: 20-70)
VOLUME_HIGH = 100 # volume in use when not typing        (recommended: 80-100)
VOLUME_END  = 100 # volume to reset to when exiting      (recommended: your default audio level)

# Delay to reenable full volume
REENABLE_DELAY = 500 # miliseconds. [accurate to 1/10s]  (recommended 200-1200)

#################################################################################
# No need to change anything below this line
#################################################################################

import sys
import re
import struct
import signal
from threading import Thread
import os
from time import sleep

# Source: https://stackoverflow.com/a/57649638
class GracefulExiter():
    def __init__(self):
        self.state = False
        signal.signal(signal.SIGINT, self.change_state)
    def change_state(self, signum, frame):
        signal.signal(signal.SIGINT, signal.SIG_DFL)
        self.state = True
    def exit(self):
        return self.state

# Global variables
flag = GracefulExiter()
number = 0
isfull = False

user = os.popen("logname").read().strip()
uid  = os.popen("id -u " + user).read().strip()


def setPulseVolumeLevel(percentage):
    rv = os.system("sudo -u " + str(user) + " env XDG_RUNTIME_DIR=/run/user/" + str(uid) + " pactl set-source-volume " + str(AUDIODEVICE) + " " + str(percentage) + "%")
    
    if rv != 0:
        print("---------------------------------------------------------------------\n"
              "Setting the microphone volume failed \n" +
              "This is probably because the AUDIODEVICE is not set correctly\n" +
              "Please open the script with a text editor and set the variable.\n" +
              "You can get a list of all possible devices using 'pactl list short sources'\n" +
              "---------------------------------------------------------------------\n")
        return
    
    print("Set input level to " + str(percentage) + "%")


def decrementor():
    global number
    global isfull
    
    while not flag.exit():
        if number > 0:
            number -= 1
            if isfull:
                setPulseVolumeLevel(VOLUME_LOW)
                isfull = False
            
        else:
            number = 0
            if not isfull:
                setPulseVolumeLevel(VOLUME_HIGH)
                isfull = True
            
        sleep(0.1)

def main(): # Modelled after https://dzone.com/articles/how-to-create-a-keylogger-for-linux-using-python
    global number
    global isfull
    global flag
    
    if os.geteuid() != 0:
        sys.exit("You need root permissions to run this script!\n" +
        "(We heavily recommend always ensuring that scripts executed as root are non-malicious by reading the source code.)")
    
    with open("/proc/bus/input/devices") as f:
        lines = f.readlines()
        pattern = re.compile("Handlers|EV=")
        handlers = list(filter(pattern.search, lines))
        
        pattern = re.compile("EV=120013")
        for idx, elt in enumerate(handlers):
            if pattern.search(elt):
                line = handlers[idx - 1]
                
        pattern = re.compile("event[0-9]*")
        infile_path = "/dev/input/" + pattern.search(line).group(0)
    
    FORMAT = 'llHHI'
    EVENT_SIZE = struct.calcsize(FORMAT)
    
    in_file = open(infile_path, "rb")
    
    event = in_file.read(EVENT_SIZE)
    
    thread = Thread(target = decrementor, args = ())
    thread.start()
    
    print("Linux automatic keyboard noise reducer ready.")
    
    # Main Keypress-Detection Loop
    while not flag.exit():
        (_,_,type,code,value) = struct.unpack(FORMAT, event)
        if code != 0 and type == 1 and value == 1:
            if isfull:
                setPulseVolumeLevel(VOLUME_LOW)
                isfull = False
            number = REENABLE_DELAY / 100
        event = in_file.read(EVENT_SIZE)
        
    # Cleanup
    thread.join()
    setPulseVolumeLevel(VOLUME_END)
    print("Exiting")

main()
