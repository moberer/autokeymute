# AutoKeyMute

A small Python script that lowers your microphone volume to avoid annoying your voice call partners.